import axios from 'axios';
import {
	fetchTennisPlayersStarted,
	fetchTennisPlayersSuccess,
	fetchTennisPlayersError
} from '../actions/tennisAPlayersActions';

export function fetchTennisPlayers() {
	return dispatch => {
		dispatch(fetchTennisPlayersStarted());
		axios
			.get('/headtohead.json')
			.then(({ data }) => dispatch(fetchTennisPlayersSuccess(data.players)))
			.catch(err => dispatch(fetchTennisPlayersError(err)));
	};
}
