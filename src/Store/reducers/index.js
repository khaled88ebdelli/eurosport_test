import { combineReducers } from 'redux'
import tennisPlayersReducer from './tenisPlayersReducer'

export default combineReducers({
    tennisPlayers: tennisPlayersReducer
})
