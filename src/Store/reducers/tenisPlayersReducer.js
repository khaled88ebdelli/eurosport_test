import {
  FETCH_TENNIS_PLAYERS_START,
  FETCH_TENNIS_PLAYERS_SUCCESS,
  FETCH_TENNIS_PLAYERS_ERROR
} from "../actions/constants/tennisPlayersTypes";

const initialState = {
  players: [],
  isFetching: false,
  error: null
};
const tennisPlayersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TENNIS_PLAYERS_START:
      return { ...state, isFetching: true };
    case FETCH_TENNIS_PLAYERS_SUCCESS:
      return {
        ...state,
        players: action.payload,
        isFetching: false
      };
    case FETCH_TENNIS_PLAYERS_ERROR:
      return { ...state, isFetching: false, error: action.payload };
    default:
      return state;
  }
};

export default tennisPlayersReducer;
