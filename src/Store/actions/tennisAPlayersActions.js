import {
	FETCH_TENNIS_PLAYERS_ERROR,
	FETCH_TENNIS_PLAYERS_START,
	FETCH_TENNIS_PLAYERS_SUCCESS
} from './constants/tennisPlayersTypes';

export const fetchTennisPlayersError = payload => {
	return { type: FETCH_TENNIS_PLAYERS_ERROR, payload };
};

export const fetchTennisPlayersStarted = () => {
	return { type: FETCH_TENNIS_PLAYERS_START };
};

export const fetchTennisPlayersSuccess = payload => {
	return { type: FETCH_TENNIS_PLAYERS_SUCCESS, payload };
};
