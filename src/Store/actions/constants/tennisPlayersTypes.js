export const FETCH_TENNIS_PLAYERS_START = "FETCH_TENNIS_PLAYERS_START";
export const FETCH_TENNIS_PLAYERS_SUCCESS = "FETCH_TENNIS_PLAYERS_SUCCESS";
export const FETCH_TENNIS_PLAYERS_ERROR = "FETCH_TENNIS_PLAYERS_ERROR";